from django.db import models
from django.utils.translation import ugettext_lazy as _


class Idioma(models.Model):
    codigo = models.CharField(_(u"Codigo del idioma"), max_length=10)
    nombre = models.CharField(_(u"Nombre del idioma"), max_length=30, default=u"", blank=True, null=True)
    nombre_general = models.CharField(_(u"Nombre de la categoria general"), max_length=100, default="General")
    nombre_papelera = models.CharField(_(u"Nombre de la categoria papelera"), max_length=100, default="Trash")
    nombre_personal = models.CharField(_(u"Nombre de la categoria personal"), max_length=100, default="Personal")
    nombre_principal = models.CharField(_(u"Nombre de la categoria principal"), max_length=100, default="Faq")

    def __unicode__(self):
        return u"%s" % self.nombre
