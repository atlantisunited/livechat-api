import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'livechat_local',
        'USER': 'livechat_local',
        'PASSWORD': 'livechat_local',
        'HOST': 'localhost',
        'PORT': '5432',
    },
    'remote': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'livechat_remote',
        'USER': 'livechat_remote',
        'PASSWORD': 'livechat_remote',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

ALLOWED_HOSTS = ['127.0.0.1']
CHAT_REDIS_URL = 'redis://localhost:6379/0'
CHAT_REDIS_CHANNEL = 'siochat'
