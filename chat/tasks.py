from celery.task import PeriodicTask
from datetime import timedelta
from core.celery import app
import socketio
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


class PeriodicTAsk(PeriodicTask):
    run_every = timedelta(minutes=50000)

    def run(self, **kwargs):
        return 'Success'


@app.task(bind=True)
def add_message_task(self, room, msg):
    """
    :param room: chat room
    :param msg: chat message
    """
    # TODO save message to DB
    sio = socketio.AsyncRedisManager(settings.CHAT_REDIS_URL, channel=settings.CHAT_REDIS_CHANNEL, write_only=True)
    sio.emit('msg', room=room, data={'data': msg}, namespace='/chat')
    return 'Success'


@app.task(bind=True)
def get_user_rooms(self, uid):
    """
    :param uid: User id
    """
    user = get_user_model().objects.get(id=uid)

    rooms = ['1', '2', '3', ]  # TODO get rooms list
    return rooms


@app.task(bind=True)
def leave_room(self, uid):
    """
    :param uid: User id
    """
    rooms = ['1', '2', '3', ]  # TODO get rooms list
    return rooms
