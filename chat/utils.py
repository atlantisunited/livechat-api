def get_session_cookie(cookie_string):
    start_cookie = cookie_string.find('sessionid')
    if start_cookie != -1:
        start_cookie += 10 # sessionid= offset
        session = cookie_string[start_cookie:start_cookie + 32]
        return session
    return False
