#!/usr/bin/env python
from aiohttp import web
import aiohttp_debugtoolbar
import logging
import socketio
from chat.tasks import add_message_task, get_user_rooms
from .utils import get_session_cookie
from django.contrib.sessions.models import Session
from django.conf import settings

LOG_FILENAME = 'debug.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)

manager = socketio.AsyncRedisManager(settings.CHAT_REDIS_URL, channel=settings.CHAT_REDIS_CHANNEL, write_only=False)
sio = socketio.AsyncServer(async_mode='aiohttp', client_manager=manager)
app = web.Application(middlewares=[aiohttp_debugtoolbar.middleware])
aiohttp_debugtoolbar.setup(app)
sio.attach(app)


# async def background_task():
#     count = 0
#     while True:
#         await sio.sleep(15)
#         count += 1
#         await sio.emit('message', {'data': 'Server generated event №{}'.format(count)}, namespace='/chat')


@sio.on('event', namespace='/chat')
async def local_message(sid, message):
    print(message)
    await sio.emit('message', {'data': message['data']}, room=sid, namespace='/chat')


# @sio.on('broadcast_event', namespace='/chat')
# async def test_broadcast_message(sid, message):
#     await sio.emit('message', {'data': message['data']}, namespace='/chat')


@sio.on('join_room', namespace='/chat')
async def join(sid, message):
    sio.enter_room(sid, message['room'], namespace='/chat')
    await sio.emit('message', {'data': 'Entered room: ' + message['room']}, room=sid, namespace='/chat')


@sio.on('leave_room', namespace='/chat')
async def leave(sid, message):
    sio.leave_room(sid, message['room'], namespace='/chat')
    await sio.emit('message', {'data': 'Left room: ' + message['room']}, room=sid, namespace='/chat')


# @sio.on('close_room', namespace='/chat')
# async def close(sid, message):
#     await sio.emit('message', {'data': 'Room ' + message['room'] + ' is closing.'}, room=message['room'],
#                    namespace='/chat')
#     await sio.close_room(message['room'], namespace='/chat')


@sio.on('room_message', namespace='/chat')
async def send_room_message(sid, message):
    add_message_task.delay(message['room'], message['data'])
    await sio.emit('message', {'data': message['data']}, room=message['room'], namespace='/chat')


@sio.on('disconnect_request', namespace='/chat')
async def disconnect_request(sid):
    await sio.disconnect(sid, namespace='/chat')


@sio.on('connect', namespace='/chat')
async def chat_connect(sid, environ):
    print(sid)
    session_key = get_session_cookie(environ['HTTP_COOKIE'])
    if session_key:  # sync user get (blocking operation)
        try:
            session = Session.objects.get(session_key=session_key)
            session_data = session.get_decoded()
            uid = session_data.get('_auth_user_id')

            # print(user)  # TODO call sync task get_user_rooms
            # rooms = get_user_rooms(uid)
            # for room in rooms:
            #     print(room)
            #     sio.enter_room(sid, room, namespace='/chat')
            # TODO move to rest call
        except Exception as e:
            print(e)
        await sio.emit('message', {'data': 'Connected', 'count': 0}, room=sid, namespace='/chat')
    else:
        await sio.disconnect(sid, namespace='/chat')


@sio.on('disconnect', namespace='/chat')
def chat_disconnect(sid):
    print('Client disconnected')
