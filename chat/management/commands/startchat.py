from django.core.management.base import BaseCommand
from chat.chatapp import sio, app  # , background_task
from aiohttp import web


class Command(BaseCommand):
    def handle(self, *args, **options):
        port = 8080
        # sio.start_background_task(background_task)
        web.run_app(app, port=port)
