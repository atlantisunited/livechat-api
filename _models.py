#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.cache import cache

from vilanding.viowner.models import UsuarioVicloning
from languages.models import Idioma
import datetime

CHAT_MSG_AUTHOR_OPERATOR = 'operador'
CHAT_MSG_AUTHOR_USER = 'usuario'

OPERATOR_STATUS_CHOICES = (
    ('on', 'Connected'),
    ('off', 'Disconnected'),
)
ROOM_STATUS_CHOICES = (

    ('void', ''),
    ('new', 'New dialogue (waiting acceptation by any operator)'),
    ('accepted', 'Active dialogue (accepted by an operator)'),
    ('reassigned', 'Reassigned to another Operator (waiting acceptation)'),
    ('closed', 'Closed dialogue'),
    ('deleted', 'Deleted dialogue'),
    ('blocked', 'Blocked dialogue'),
)


class OperatorConnection(models.Model):
    usuario_vicloning = models.ForeignKey(UsuarioVicloning, verbose_name=u"UsuarioVicloning")
    status = models.CharField(u"Status", max_length=25, choices=OPERATOR_STATUS_CHOICES, default='off')
    max_rooms = models.IntegerField(u"Rooms_max", default=1, max_length=25)
    connects_rooms = models.IntegerField(u"total Rooms", default=0, max_length=25)
    time_connect = models.IntegerField(u"segundos conectado", default=0, max_length=25)
    time_temp = models.CharField(u"tiempo_temporal", max_length=150, default=u"")

    def set_status(self, status):
        if not self.status == status:
            self.status = status
            self.save()
        return self.status

    def add_connect_room(self):
        self.connects_rooms = self.connects_rooms + 1
        return self.connects_rooms

    def del_connect_room(self):
        self.connects_rooms = self.connects_rooms - 1
        return self.connects_rooms


@receiver(post_save, sender=OperatorConnection)
def OperatorConnection_post_save(sender, instance, created, **kwargs):
    if instance.status == "on":
        for contador in range(0, (instance.max_rooms - instance.connects_rooms)):
            """
            temp_room=Room(
                operator=instance,
                idioma="",
                user_name="",
                socket_key="",
                )
            temp_room.save()
            """
    else:
        pass
        # instance.time_connect=time_connect+(datetime.datetime.today()-instance.time_temp)


# sala son las salas de CHAT que se han creado, tambien se crean salas enviando un mail
# una sala es el canal de comunicacion con el cliente
class Sala(models.Model):
    owner = models.ForeignKey(UsuarioVicloning, verbose_name=u"Owner (not simply operator)", default=None, blank=True,
                              null=True)
    operador = models.CharField(u"Operador", max_length=250, default="")
    idioma = models.CharField(u"Idioma", max_length=250, default="")
    user_name = models.CharField(u"User name", max_length=100, default=u"", blank=True, null=True)
    user_email = models.CharField(u"User email", max_length=100, default=u"", blank=True, null=True)
    socket_key = models.CharField(u"websocket id", max_length=100, default=u"", blank=True, null=True)
    status = models.CharField(u"Status", max_length=25, choices=ROOM_STATUS_CHOICES, default='void')
    created = models.DateTimeField(editable=False, auto_now_add=True, default=datetime.datetime.today())
    ultimo_mensaje = models.ForeignKey('Dialogos', verbose_name=u"Ultimo mensaje", default=None, null=True)
    ultima_visita = models.ForeignKey('Visita', verbose_name=u"Ultima visita", default=None, null=True)
    rate = models.IntegerField('Rate', default=None, null=True)

    def last_event_date(self):
        # returns datetime (creation time of last object belonging to this room -either Dialogos or the Room itself-)
        messages = self.dialogos_set
        if messages.exists():
            last_date = messages.order_by('-created')[0].created
        else:
            last_date = self.created
        return last_date

    def duration(self):
        duration = (self.last_event_date() - self.created).total_seconds()
        return duration

    def copy_dialogue(self, origin_room):
        # Insert a copy of all Dialogos instances of 'origin_room' into this Room
        for msg in origin_room.dialogos_set.all():
            self.dialogos_set.create(
                    author=msg.author,
                    content=msg.content,
                    created=msg.created,
            )

    @property
    def sesion(self):
        try:
            return self.socket_key
        except:
            return ''

    def __unicode__(self):
        return u"{0}_{1}".format(self.operador, self.socket_key)


# visita son las urls que te muevess
class Visita(models.Model):
    owner = models.ForeignKey(UsuarioVicloning, verbose_name=u"Owner (not simply operator)", default=None, blank=True,
                              null=True)
    room = models.ForeignKey(Sala, verbose_name=u"Room")
    url = models.CharField(u"URL", max_length=250, default=u"", blank=True, null=True)
    title = models.CharField(u"Titulo", max_length=250, default=u"", blank=True, null=True)
    idioma = models.CharField(u"Idioma", max_length=250, default="")
    ip = models.CharField(u"IP", max_length=250, default=u"", blank=True, null=True)
    city = models.CharField(u"City", max_length=250, default=u"", blank=True, null=True)
    country = models.CharField(u"Country", max_length=250, default=u"", blank=True, null=True)
    browser = models.CharField(u"Browser", max_length=250, default=u"", blank=True, null=True)
    sesion_user = models.CharField(u"Sesion", max_length=100, default=u"", blank=True, null=True)
    created = models.DateTimeField(db_index=True, editable=False, auto_now_add=True, default=datetime.datetime.today())
    device = models.CharField(u"Device", max_length=250, default=u"", blank=True, null=True)


@receiver(post_save, sender=Visita)
def visita_post_save(sender, instance, created, **kwargs):
    instance.room.ultima_visita = instance
    instance.room.save()


class Dialogos(models.Model):
    room = models.ForeignKey(Sala, verbose_name=u"Room")
    author = models.CharField(u"Author of message", max_length=100)  # CHAT_MSG_AUTHOR_OPERATOR or CHAT_MSG_AUTHOR_USER
    content = models.TextField(u"Message content", default=u"", blank=True, null=True)
    created = models.DateTimeField(db_index=True, editable=False, auto_now_add=True, default=datetime.datetime.today())
    new = models.BooleanField(u"New", default=True)
    url = models.CharField(u"URL", max_length=250, default=u"", blank=True, null=True)
    ip = models.CharField(u"IP", max_length=250, default=u"", blank=True, null=True)
    city = models.CharField(u"City", max_length=250, default=u"", blank=True, null=True)
    country = models.CharField(u"Country", max_length=250, default=u"", blank=True, null=True)
    browser = models.CharField(u"Browser", max_length=250, default=u"", blank=True, null=True)
    sesion_user = models.CharField(u"Sesion", max_length=100, default=u"", blank=True, null=True)
    operador = models.CharField(u"Operador", max_length=250, default="")
    device = models.CharField(u"Device", max_length=250, default=u"", blank=True, null=True)  #
    template = models.CharField(u"Template", max_length=250, default=u"", blank=True, null=True)
    fbid = models.CharField(u"fbid", max_length=250, default=u"", blank=True, null=True)  # facebook id
    # meta=models.CharField(u"meta", max_length=30,default=u"", blank=True, null=True) #external data for example browser=email, meta=received Mail recibido
    reply = models.BooleanField(u"New",
                                default=True)  # si es una respuesta a un mensaje, por ejemplo una respuesta a un correo Re: o bien una respuesta a una pregunta de un agente que sirva luego para entrenar dialogos

    def __unicode__(self):
        return u"{0}_{1}".format(self.author, self.created)


@receiver(post_save, sender=Dialogos)
def dialogos_post_save(sender, instance, created, **kwargs):
    if instance.author != 'CHAT_MSG_INTERNAL':
        url_key = "<owner_{0}>-HISTORICO-{1}".format(instance.room.owner.id, instance.room.id)
        url_key2 = "<owner_{0}>-HISTORICO-{1}".format(instance.room.owner.id, instance.sesion_user)
        historico1 = cache.get(key=url_key, default=[])
        historico2 = cache.get(key=url_key, default=[])
        if len(historico1) > len(historico2):
            historico = historico1
        else:
            historico = historico2
        # cache.delete(url_key)
        # print historico
        data = {"emisor": instance.author, "texto": instance.content, "fecha": str(instance.created)}
        if data not in historico:
            # print datetime.datetime.now(),instance.created
            if (datetime.datetime.now() - instance.created).seconds < 15:
                historico.append(data)
                cache.set(key=url_key, value=historico, timeout=20)
                cache.set(key=url_key2, value=historico, timeout=20)
                # if instance.author!='CHAT_MSG_AUTHOR_USER':
    if created:
        instance.room.ultimo_mensaje = instance
        instance.room.save()


class PreguntaChat(models.Model):
    usuario_vicloning = models.ForeignKey(UsuarioVicloning, verbose_name="User")
    idioma = models.ForeignKey(Idioma, verbose_name="Language")
    pregunta = models.TextField("Question", default=u"", null=True, blank=True)
    respuesta = models.TextField("Answer", default=u"", null=True, blank=True)


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}-{2}'.format(instance.dialogo.room.owner, instance.dialogo.id, filename)


class UserFile(models.Model):
    dialogo = models.ForeignKey(Dialogos)
    upload = models.FileField(upload_to=user_directory_path)
