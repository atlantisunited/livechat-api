# -*- coding: utf-8 -*-
# from __future__ import print_function
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import permission_required, login_required
from django.http import HttpResponseRedirect, Http404, HttpResponse
from decorators import checksteps
from django.conf import settings
from interface.models import JOB_TITLE, GOAL, DEFAULT_AVATAR_MARKETEER
from vilanding.viowner.models import DEFAULT_AVATAR_USER
import simplejson
from languages.models import Idioma
# , CHAT_MSG_AUTHOR_USER
from chat.models import Sala, Dialogos, OperatorConnection, CHAT_MSG_AUTHOR_OPERATOR, Visita, UserFile
from common.utils import encriptar_user, desencriptar_user
from django.views.decorators.cache import never_cache
from django.core.cache import cache
import json
from chat_v2.utils_cache import *
from chat_v2.utils_livechat import get_operador_owner_marketeer, get_padre, get_url, get_ip, get_params, get_tittle_url
from leads_app.models import GruposLeads, Contacto, Info, PRIORITY_CHOICES, STATUS_CHOICES
from django.core import serializers
from vilanding.viowner.models import UsuarioVicloning
import datetime
from django.db.models import Q
import humanize
import datetime
from dateutil import parser
from django.contrib.gis.geoip import GeoIP
from user_agents import parse
from stats.models import Sesion, PreguntaUsuario, FaqListada, FaqClicada, \
    LinkMostrado, ProductoMostrado, Valoracion, TriggerLanzado, \
    UserEvent, SearchUsuario
import phonenumbers
from phonenumbers import carrier
from phonenumbers import geocoder
from django.views.decorators.csrf import csrf_exempt
from administrador2.htmltotext import *
from administrador2.utils import *
from ipwhois import IPWhois
from ipwhois.utils import get_countries
from django_countries import countries
from automations.models import AM_EmailTemplates
from viclone.models import Ambito, AmbitoIdioma
from vifaqs.models import Faq, FaqIdioma, Equivalente
import nltk
from nbest.utils import add_equivalente, add_faqidioma
import requests
from facebook_app.models import Facebook
from bs4 import BeautifulSoup as BS
# import timedelta
import html2text
from image_manager import b64encoded_img2file
import os
import time
import sys
from administrador2.leads import filter_infos
from django.db.models import Sum, Count
import urllib
from shortener.models import Urls

reload(sys)
sys.setdefaultencoding('utf8')


@login_required(login_url=reverse_lazy("login"))
@checksteps()
# @permission_required("vifaqs.change_faqidioma" ,  login_url =reverse_lazy("access_denied") )
def panel_v2(request):
    ####test performance
    timeIt = ''
    import time
    start_time = time.time()
    #####print("--- %s seconds ---" % (time.time() - start_time))

    from supportcenter.views import autocomplete
    params = request.GET
    if 'user' in params:
        user_selected = params['user']
    else:
        user_selected = ''
    if 'id_message' in params:
        id_message = params['id_message']
    else:
        id_message = ''

    owner = request.usuario_vicloning
    usuario = request.usuario_vicloning
    while owner.padre:
        owner = owner.padre
    operator, created = usuario.operatorconnection_set.get_or_create(
    )
    operators_disponibles = usuario.hijos.all()
    salas = owner.sala_set.filter(status="accepted")
    ###performance
    # print '1. livechat, ' + str((time.time() - start_time))
    start_time = time.time()
    ####
    historia_total = []
    for sala in salas:
        dialogos_temp = sala.dialogos_set.values().order_by('created')

        dialogo = []
        for dialogo_temp in dialogos_temp:
            dialogo.append({
                'content': dialogo_temp["content"],
                'room_id': dialogo_temp["room_id"],
                'id': dialogo_temp["id"],
                'author': dialogo_temp["author"],
                'created': dialogo_temp["created"].strftime('%a %d-%h-%Y %I:%M:%S %P'),
            }),

        historial = {
            "id": sala.id,
            "user_name": sala.user_name,
            "user_email": sala.user_email,
            "sesion": sala.sesion,
            "idioma": sala.idioma,
            "operador": sala.operador,
            "owner_id": sala.owner_id,
            "dialogos": dialogo
        }

        historia_total.append(historial)

    ###performance
    # print '2. livechat, ' + str((time.time() - start_time))
    start_time = time.time()
    ####
    estado_operadores = []
    operators_disponibles = [usuario for usuario in owner.hijos.all()]
    operators_disponibles.append(owner)
    usu_key = encriptar_user(owner.id)
    for operador in operators_disponibles:
        estado_operadores.append({
            'username': operador.user.username,
            'avatar': operador.avatar,
            'first_name': operador.user.first_name,
            'last_name': operador.user.last_name,
            'id': operador.id,
            'status': OperatorConnection.objects.filter(usuario_vicloning=operador)[0].status
        })
    ###performance
    # print '3. livechat, ' + str((time.time() - start_time))
    start_time = time.time()
    ####
    # el follon del texto de bienvenida del chat
    from interface.models import Text
    if usuario.chat_welcome_agent_msg != '':
        welcome_message = usuario.chat_welcome_agent_msg.replace('%%MYNAME%%', request.user.first_name)
    else:
        text = owner.assistant.get_text(idioma=owner.idioma_base).get_values()
        welcome_message = text['chat_welcome_agent_msg'].replace('%%MYNAME%%', request.user.first_name)

    ###performance
    # print '4. livechat, ' + str((time.time() - start_time))
    start_time = time.time()
    ####
    # print 'usuario.idioma_base::',owner.idioma_base,'::',welcome_message,':::',request.user.first_name

    ctx = {
        'usuario': usuario,
        'operator': operator,
        'operators_disponibles': operators_disponibles,
        'url_chat_server': settings.URL_CHAT_SERVER,
        'usu_key': usu_key,
        'owner': owner,
        'historial': json.dumps(historia_total),
        'subdominio': owner.user.username,  # username
        'estado_operadores': estado_operadores,
        'user_selected': user_selected,
        'id_message': id_message,
        # filters
        'welcome_message': welcome_message,
        'tags': etiquetas(request=request, interno=True),  # lento 'tags',
        'groups': GruposLeads.objects.filter(usuario_vicloning=owner),
        'STATUS_CHOICES': STATUS_CHOICES,
        'PRIORITY_CHOICES': PRIORITY_CHOICES,
    }
    ###performance
    # print '5. livechat, ' + str((time.time() - start_time))
    # start_time = time.time()
    ####
    return render_to_response('livechat_v2.html', RequestContext(request, ctx))


def cliente(request):
    operator, created = request.usuario_vicloning.operatorconnection_set.get_or_create(
    )

    ctx = {
        'operator': operator,
    }
    return render_to_response('livechat_cliente.html', RequestContext(request, ctx))


def operators_available(request, json=False):
    owner = request.usuario_vicloning
    usuario = request.usuario_vicloning
    while owner.padre:
        owner = owner.padre
    # operador, owner, marketeer = get_operador_owner_marketeer(request)
    marketeer = owner.user.username
    avatar = owner.avatar
    estado_operadores = []
    for operador in owner.hijos.all():
        if (OperadorIsOnline(marketeer, operador) == 'on'):
            avatar = operador.avatar
        else:
            pass
            # disconnect_operator(request, operador)
        estado_operadores.append({
            'username': operador.user.username,
            'avatar': operador.avatar,
            'first_name': operador.user.first_name,
            'last_name': operador.user.last_name,
            'id': operador.id,
            'status': OperadorIsOnline(marketeer, operador),
        })
    ctx = {
        'estado_operadores': estado_operadores
    }
    if json:
        return avatar
    return render_to_response('operators_available.html', RequestContext(request, ctx))


@login_required(login_url=reverse_lazy("login"))
@checksteps()
# @permission_required("chat.add_room" ,  login_url =reverse_lazy("access_denied") )
def soundAlert(request):
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    if 'sound' in request.GET.keys():
        print
        'cambiando datos', request.GET['sound']
        operador.soundAlert = int(request.GET['sound'])
    if 'activateDesktop' in request.GET.keys():
        print
        'cambiando datos', request.GET['activateDesktop']
        operador.activateDesktop = request.GET['activateDesktop'] == 'true'
        # return HttpResponse(simplejson.dumps(operador.activateDesktop))
    operador.save()
    return HttpResponse(simplejson.dumps(''))


@login_required(login_url=reverse_lazy("login"))
@checksteps()
# @permission_required("chat.add_room" ,  login_url =reverse_lazy("access_denied") )
def connect_operator(request, op_id=''):
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    operadoraux = OperatorConnection.objects.get(
            usuario_vicloning=operador)
    operador_online(marketeer, operador, 'on')
    operadoraux.status = 'on'
    operadoraux.save()

    operador_online(marketeer, operador, 'on')
    return HttpResponse(simplejson.dumps(OperadorIsOnline(marketeer, operador), operadoraux.status))


@login_required(login_url=reverse_lazy("login"))
@checksteps()
# @permission_required("chat.add_room" ,  login_url =reverse_lazy("access_denied") )
def disconnect_operator(request, operador=''):
    operador1, owner, marketeer = get_operador_owner_marketeer(request)
    if operador:
        operator = OperatorConnection.objects.get(usuario_vicloning=operador)
    else:
        operator = OperatorConnection.objects.get(usuario_vicloning=operador1)

    operador_online(marketeer, operador1, 'off')
    operador_online(marketeer, operator, 'off')
    operator.status = 'off'
    operator.save()
    return HttpResponse(simplejson.dumps(OperadorIsOnline(marketeer, operador1), operator.status))


def merge_sala(sala1, sala2):
    if sala1 != sala2:
        try:
            # Dialogos.objects.filter(room=sala2).update(room=sala1)
            for dialogo in Dialogos.objects.filter(room=sala2):
                dialogo.room = sala1
                dialogo.save()
        except:
            pass
        try:
            # Visita.objects.filter(room=sala2).update(room=)
            for visita in Visita.objects.filter(room=sala2):
                visita.room = sala1
                visita.save()
        except:
            pass
        sala2.delete()
    return sala1


def prepara_infos(infos, lista_online, lista_activos, num, listed='min'):
    lista = []
    # poniendo el orden a los infos
    try:
        infos = infos.annotate(null_position=Count('sala_asignada__ultimo_mensaje')).order_by("-mensajes_sin_leer",
                                                                                              "-null_position",
                                                                                              "-sala_asignada__ultimo_mensaje__created",
                                                                                              "-sala_asignada__ultima_visita__created")
        # infos=infos.annotate(null_position=Count('sala_asignada__ultima_visita')).order_by("-mensajes_sin_leer","-sala_asignada__ultimo_mensaje__created","-null_position","-sala_asignada__ultima_visita__created")
        # infos=infos.order_by("-mensajes_sin_leer","-sala_asignada__ultimo_mensaje__created")
    except:
        infos = infos

    for info in infos[:num]:
        # REPARAN INFOS:
        if info.mensajes_sin_leer == None:
            info.mensajes_sin_leer = 0
            info.save()
        ####
        try:
            sala = info.sala_asignada
        except:
            continue
        try:
            country = dict(countries)[info.country.code]
        except:
            country = ''
        info_to_append = {
            'pk': info.pk,
            'nombre': info.nombre,
            'last_name': info.last_name,
            'company': info.company,
            'email': info.email if info.email else '',
            'contact_id': info.id,
            'operador': info.sala_asignada.operador,
            'avatar': info.avatar,
            'archivado': sala.status == 'closed',
            'eliminado': sala.status == 'deleted',
            'status': info.status,
            'prioridad': info.priority,
            # 'estado': info.status2, #devuelve si el usaurio esta online / offline / activo
            'mensaje': '',
            'time_to_sort': '',
            'time': '',
            'ip': '',
            'city': info.city,
            'country': country,
            'userLost': info.userLost,
            'idioma': info.idioma,
            'mensajes_sin_leer': info.mensajes_sin_leer,
        }
        if listed == 'max':  # lista extendida
            info_to_append['tags'] = info.tags

        if info.id in lista_online:
            info_to_append['estado'] = 'online'
            info_to_append['status_key'] = 0
        elif info.id in lista_activos:
            info_to_append['estado'] = 'activo'
            info_to_append['status_key'] = 1
        else:
            info_to_append['estado'] = 'offline'
            info_to_append['status_key'] = 2
        # info_to_append = json.loads(serializers.serialize('json', [info]))[0]['fields'] #LENTO

        """info_to_append.update({
                                    #'contact_id': info.id,
                                    'status_key': info.status_key,
                                    'operador': info.sala_asignada.operador,
                                    'avatar': info.avatar,
                                    'archivado': sala.status == 'closed',
                                    'eliminado': sala.status == 'deleted',
                                    'status': info.status,
                                    'prioridad': info.priority,
                                    'estado': info.status2, #devuelve si el usaurio esta online / offline / activo
                                    'mensaje': '',
                                    'time_to_sort': '',
                                    'time': '',
                                    'ip': '',
                                    'city': '',
                                    'country': '',
                                })"""
        if info.email == '' and info.nombre == '':
            info_to_append['nombre'] = 'Visitor'
            info_to_append['email'] = ''
        visita = sala.ultima_visita
        if visita != None:
            if visita.created != '':
                info_to_append['time_to_sort'] = str(visita.created)
            if visita.browser:
                info_to_append['browser'] = visita.browser
            if visita.ip:
                info_to_append['ip'] = visita.ip
            if visita.city:
                info_to_append['city'] = visita.city
            if visita.country:
                info_to_append['country'] = visita.country
            if visita.url:
                info_to_append['URL'] = visita.url
            if visita.title:
                info_to_append['titleURL'] = visita.title

        ultimo_mensaje = sala.ultimo_mensaje
        if ultimo_mensaje != None:
            if (datetime.datetime.now().strftime("%d/%m/%y") == ultimo_mensaje.created.strftime("%d/%m/%y")):
                time_to_append = ultimo_mensaje.created.strftime("%H:%M:%S")
            else:
                time_to_append = ultimo_mensaje.created.strftime(
                        "%d/%m/%y (%H:%M:%S)")
            if ultimo_mensaje.country != '':
                info_to_append['country'] = ultimo_mensaje.country
            if str(ultimo_mensaje.created) != '':
                info_to_append['time_to_sort'] = str(ultimo_mensaje.created)
            if ultimo_mensaje.city:
                info_to_append['city'] = ultimo_mensaje.city
            info_to_append.update({
                'mensaje': ultimo_mensaje.content[:20],
                # 'time_to_sort': str(ultimo_mensaje.created),
                'browser': ultimo_mensaje.browser,
                'ip': ultimo_mensaje.ip,
                # 'city': ultimo_mensaje.city,
                # 'country': ultimo_mensaje.country,
                'time': time_to_append,
            })
        lista.append(info_to_append)
    return lista


@csrf_exempt
@login_required(login_url=reverse_lazy("login"))
@checksteps()
# @cache_jquery
def usuarios_online(request):
    from django.db import connection
    connection._rollback()
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    operador_connection = OperatorConnection.objects.get(usuario_vicloning=operador)
    params = get_params(request)
    Visitors, New, Active, Offline, Assigned, Archived, Deleted = False, False, False, False, False, False, False
    new_unread_visitors, new_unread_new, new_unread_activos, new_unread_offline, new_unread_facebook, new_unread_general, new_unread_assigned, gente_online = 0, 0, 0, 0, 0, 0, 0, 0
    dataArchived, dataDeleted, dataActive, dataNuevos, dataOffline, dataAsigned, dataVisitors, dataFB = [], [], [], [], [], [], [], []
    operador_online(marketeer, operador, operador_connection.status)

    lista_online = whoIsOnline(marketeer)
    lista_activos = whoIsActive(marketeer)

    # OPERADORES:
    operadores_u = UsuarioVicloning.objects.filter(Q(padre=operador) | Q(padre=owner)).exclude(
            padre__isnull=True) | UsuarioVicloning.objects.filter(user=owner.user)
    operadores = OperatorConnection.objects.filter(
            usuario_vicloning__in=operadores_u)
    dataOperadores = []
    for operadoraux in operadores:
        if (OperadorIsOnline(marketeer, operadoraux.usuario_vicloning) == 'on'):
            operatorsOnline = True
            status = 'on'
        else:
            status = 'off'
        dataOperadores.append({
            'id': operadoraux.usuario_vicloning.id,
            'nombre': operadoraux.usuario_vicloning.user.first_name + ' ' + operadoraux.usuario_vicloning.user.last_name,
            'status': status,
            'avatar': str(operadoraux.usuario_vicloning.avatar)
        })

    ########<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    infos = Info.objects.filter(owner=owner).exclude(sala_asignada=None)
    infos_block = infos.filter(sala_asignada__status='blocked')
    infos = infos.exclude(sala_asignada__status='blocked')
    infos_visitors = infos.filter(id__in=lista_online)

    infos = infos.exclude(nombre='', email='', sala_asignada__user_name='', sala_asignada__user_email='',
                          facebookid='')  # Info.objects.filter(owner=owner).exclude(sala_asignada=None,sala_asignada__status='blocked').exclude(nombre='',email='',sala_asignada__user_name='',sala_asignada__user_email='',facebookid='')

    # print infos[2000].__dict__
    infos = filter_infos(owner=owner, infos=infos, name=request.POST.get('searchName', ''), request=request.POST,
                         orderby=request.POST.get('orderby', 'nombre'))
    pagina = int(params.get('page', 1))
    # print 'page',pagina
    filtro = params.get('filtro', '')  # saber si ha picado a alguna lista para ver o es INBOX
    listed = params.get('listed', 'min')  # saber si es lista reducida o lista maximizada
    infos_deleted = infos.filter(sala_asignada__status='deleted')
    infos_archived = infos.filter(sala_asignada__status='closed')
    infos_new = infos.exclude(facebookid='', email='').filter(sala_asignada__operador="",
                                                              mensajes_sin_leer__gt=0).exclude(
        sala_asignada__status='closed').exclude(sala_asignada__status='deleted')
    infos_activos = infos.filter(id__in=lista_activos).exclude(facebookid='', email='').exclude(
        sala_asignada__status='closed').exclude(sala_asignada__status='deleted').filter(
        sala_asignada__operador=str(operador.id))
    infos_offline = infos.exclude(id__in=lista_activos).filter(sala_asignada__operador=str(operador.id)).exclude(
        facebookid='', email='').exclude(sala_asignada__status='closed').exclude(sala_asignada__status='deleted')
    infos_assigned = infos.filter(
        sala_asignada__operador__in=[str(operadoraux.usuario_vicloning.id) for operadoraux in operadores]).exclude(
        sala_asignada__operador=operador.id).exclude(sala_asignada__status='closed').exclude(
        sala_asignada__status='deleted')  #
    infos_fb = infos.exclude(facebookid='').exclude(sala_asignada__status='deleted')
    infos_bots = infos.filter(sala_asignada__operador="").exclude(sala_asignada__status='closed').exclude(
        sala_asignada__status='deleted').exclude(facebookid='', email='')

    infos_unread_messages = infos.filter(
        Q(sala_asignada__operador=str(operador.id)) | Q(sala_asignada__operador="")).filter(
        mensajes_sin_leer__gt=0).exclude(sala_asignada__status='closed').exclude(sala_asignada__status='deleted')

    # print infos_new.count(),'|',infos_deleted.count(),'|',infos_activos.count(),'|',infos_assigned.count(),'|',infos_visitors.count(),'|'
    # NUMEROS:

    num_deleted = infos_deleted.count()
    num_archived = infos_archived.count()
    num_active = infos_activos.count()
    num_new = infos_new.count()
    num_offline = infos_offline.count()
    num_assigned = infos_assigned.count()
    num_visitors = infos_visitors.count()
    num_fb = infos_fb.count()
    num_block = infos_block.count()
    num_bots = infos_bots.count()
    new_unread_visitors = infos_visitors.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    new_unread_new = infos_new.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    new_unread_activos = infos_activos.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    new_unread_offline = infos_offline.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    new_unread_assigned = infos_assigned.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    new_unread_fb = infos_fb.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    new_unread_block = infos_block.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    new_bots = infos_bots.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    infos_new_unread_total = infos_visitors | infos_new | infos_activos | infos_assigned | infos_fb
    new_unread_total = infos_new_unread_total.aggregate(Sum('mensajes_sin_leer'))["mensajes_sin_leer__sum"] or 0
    # print infos.filter(mensajes_sin_leer__gt=0)[0]


    elementos_por_pagina = 20
    data = {'deleted': [], 'archivados': [], 'activos': [], 'new': [], 'offline': [], 'assigned': [], 'visitors': [],
            'fb': []}
    # DELETED/ARCHIVED/RESTO
    if filtro != 'seeDeleted' and filtro != 'seeArchived':
        # Si no son Deleted ni Archived, los excluimos para el resto
        infos = infos.exclude(sala_asignada__status='closed')
        infos = infos.exclude(sala_asignada__status='deleted')
    elif filtro == 'seeArchived':
        data['archivados'] = prepara_infos(infos_archived, lista_online, lista_activos, pagina * elementos_por_pagina,
                                           listed)
    elif filtro == 'seeDeleted':
        data['deleted'] = prepara_infos(infos_deleted, lista_online, lista_activos, pagina * elementos_por_pagina,
                                        listed)

    if filtro == 'seeNew':
        # Online al final son los nuevos, a ver si le cambiamos el nombre
        data['New'] = prepara_infos(infos_new, lista_online, lista_activos, pagina * elementos_por_pagina, listed)
    # PROPIOS/ASIGNADOS/NO ASIGNADOS
    if filtro != 'seeAssigned':
        # Solo muestro los mios, a no ser q filtre por asignados o que no esten asignados a nadie
        infos = infos.filter(Q(sala_asignada__operador=str(operador.id)) | Q(sala_asignada__operador=""))
    elif filtro == 'seeAssigned':
        infos = infos.filter(
            sala_asignada__operador__in=[str(operadoraux.usuario_vicloning.id) for operadoraux in operadores])
        data['assigned'] = prepara_infos(infos_assigned, lista_online, lista_activos, pagina * elementos_por_pagina,
                                         listed)

    # VISITORS/ACTIVOS/OFFLINE
    if filtro == 'seeVisitors':
        # Solo los que estan en lista activos y el email = '' y el fb =''
        data['visitors'] = prepara_infos(infos_visitors, lista_online, lista_activos, pagina * elementos_por_pagina,
                                         listed)

    if filtro == 'seeActive':
        data['activos'] = prepara_infos(infos_activos, lista_online, lista_activos, pagina * elementos_por_pagina,
                                        listed)
    if filtro == 'seeOffline':
        sala_asignada__operador = str(operador.id)
        data['offline'] = prepara_infos(infos_offline, lista_online, lista_activos, pagina * elementos_por_pagina,
                                        listed)
    if filtro == 'seeFacebook':
        data['fb'] = prepara_infos(infos_fb, lista_online, lista_activos, pagina * elementos_por_pagina, listed)
    if filtro == 'seeBlock':
        data['block'] = prepara_infos(infos_block, lista_online, lista_activos, pagina * elementos_por_pagina, listed)
    if filtro == 'seeBots':
        data['bots'] = prepara_infos(infos_bots, lista_online, lista_activos, pagina * elementos_por_pagina, listed)

    if filtro == '':
        data['New'] = prepara_infos(infos_new, lista_online, lista_activos, pagina * elementos_por_pagina, listed)
        if len(data['New']) < pagina * elementos_por_pagina:
            data['activos'] = prepara_infos(infos_activos, lista_online, lista_activos, pagina * elementos_por_pagina)
            # data['visitors']=prepara_infos(infos_visitors,lista_online,lista_activos,pagina*elementos_por_pagina,listed)
            # if (len(data['New'])+len(data['visitors']))<pagina*elementos_por_pagina:
            if (len(data['New']) + len(data['activos'])) < pagina * elementos_por_pagina:
                data['offline'] = prepara_infos(infos_offline, lista_online, lista_activos,
                                                pagina * elementos_por_pagina, listed)

    ##########<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    ##TODO: limites en numeros para mostrarlos, orden


    if 'dash' in request.GET.keys():
        # print 'operador:',operador
        user_total_online = (infos_visitors | infos_new)
        usuarios = {
            'imonline': OperadorIsOnline(marketeer, operador),
            # OperatorConnection.objects.get(usuario_vicloning=operador).status,
            'user_total_online': user_total_online.count(),  # total_online
            'user_new': num_new,  # news
            'user_online': num_active,  # online
            'pending_training': len(cache.keys("<owner_{0}>-FAQSAPIFAQ*".format(owner.id))),  # entrenando
            'messages_unread': new_unread_total,  # mensajes sin leer
            'messages_notify': alert_operator_list(owner.id, operador),  # notify
            'operator_list': (dataOperadores),  # operadores
            'messages_unread_users': prepara_infos(infos_unread_messages, lista_online, lista_activos,
                                                   pagina * elementos_por_pagina, listed),
            # infos_unread_messages
            'notify_sound': operador.soundAlert,
            'notify_desktop': operador.activateDesktop,
        }
        response = HttpResponse(json.dumps(usuarios), content_type="application/json")
        response['Access-Control-Allow-Origin'] = "*"
        return response

    nums = {}
    usuarios = {
        'deleted': [],
        'archivados': [],
        'activos': [],
        'new': [],
        'offline': [],
        'assigned': [],
        'visitors': [],
        'bots': [],
        'block': [],
        'operators': (dataOperadores),
        'new_unread_block': new_unread_block,
        'new_unread_new': new_unread_new,
        'new_unread_visitors': new_unread_visitors,
        'new_unread_activos': new_unread_activos,
        'new_unread_offline': new_unread_offline,
        'new_unread_assigned': new_unread_assigned,
        'new_unread_fb': new_unread_fb,
        'PRIORITY_CHOICES': PRIORITY_CHOICES, 'STATUS_CHOICES': STATUS_CHOICES,
        'timeIt': 'timeIt',
        'num_deleted': num_deleted,
        'num_archived': num_archived,
        'num_active': num_active,
        'num_new': num_new,
        'num_block': num_block,
        'num_offline': num_offline,
        'num_assigned': num_assigned,
        'num_visitors': num_visitors,
        'num_bots': num_bots,
        'num_fb': num_fb,
        'notify': alert_operator_list(owner.id, operador),
        # si hay alertas que mandar a los operadores por usuarios nuevos online
    }


for key in data.keys():
    usuarios[key] = data[key]

return HttpResponse(json.dumps(usuarios), content_type="application/json")


@csrf_exempt
@cache_angular
def online(request):
    # ------------------------------------
    # Desencripta el id de usuario:
    params = json.loads(request.POST.keys()[0])
    usuario = desencriptar_user(params['marketeer'])  # id de UsuarioVicloning
    owner = UsuarioVicloning.objects.get(id=usuario)
    mktrid = 'mktr' + str(owner.id)
    sesion = request.COOKIES.get(mktrid)
    referrer = request.META['HTTP_REFERER']

    # print 'usuario',usuario,':::owner:',owner,':::mktrid:',mktrid,':::sesion:',sesion
    if not request.COOKIES.get(mktrid):
        sesion = params['sesion']
    ip = get_ip(request)
    url = get_url(request)
    email = params['email'].replace(' ', '+')
    name = params.get('name', '')
    lang = params.get('lang', '')
    chat = params.get('chat', '')
    userLost = params.get('userLost', False)
    # Obtiene la sala de chat

    id_woodpecker = re.findall('openIdUrl=(.*?)\&', urllib.unquote(params['docref']), re.DOTALL)
    if id_woodpecker:
        try:
            apykey_woodpecker = ''
            url = 'https://api.woodpecker.co/rest/v1/click?openIdUrl=' + id_woodpecker[0]
            response = requests.get(url, auth=(apykey_woodpecker, 'pass'))
            if response.ok:
                woodpecker_data = json.loads(response.content)
                for data in woodpecker_data:
                    email = data['email']
                    name = data['first_name'] + ' ' + data['last_name']
                    try:
                        defaults = {
                            'nombre': data['first_name'],
                            'last_name': data['last_name'],
                            "company": data["company"],
                            # "industry":data["industry"],
                            "web": data["website"],
                            # "tags":data["tags"],
                            "title": data["title"],
                            "phone": data["phone"],
                            "address": data["address"],
                            "city": data["city"],
                            # "state":data["state"],
                            "country": data["country"],
                        }

                        info = Info.objects.filter(email=email, owner=owner)
                        if len(info) > 0:
                            info = info[0]
                        else:
                            info = Info(email=email, owner=owner)

                        for key, value in defaults.items():
                            if info.__dict__[key] == '':
                                info.__dict__[key] = defaults[key]
                        tags_behaviour = info.tags.split(',')
                        tags_behaviour.remove('')
                        if data['campaign_name'] not in tags_behaviour:
                            tags_behaviour.append(data['campaign_name'])
                        if 'Woodpecker' not in tags_behaviour:
                            tags_behaviour.append('Woodpecker')

                        info.tags = ','.join(tags_behaviour)
                        info.save()
                        chat_online, hasMail = usuario_online(sesion, owner, info.email, name=info.nombre)


                    except:
                        chat_online, hasMail = usuario_online(sesion, owner, data['email'], name=name)


                        # "campaign_id": 7619,
                        # "campaign_name": "Campaign #18",
                        # "campaign_email": "woodie@woodpeckerhq.com",
                        # "campaign_status": "STOPPED",
        except:
            pass

    # print ':::sesion:',sesion,'/email/',email,'/name/',name,'/lang/',lang,'/chat/',chat,'/userLost/',userLost

    operadores = OperatorConnection.objects.filter(status='on', usuario_vicloning__in=(UsuarioVicloning.objects.filter(
            Q(padre=owner) | Q(user=owner.user))))
    operatorsOnline = False
    for operador in operadores:
        # print OperadorIsOnline(owner.user.username, operador.usuario_vicloning),'<<<<-----',operador.usuario_vicloning.id
        # print owner.user.username, operador.usuario_vicloning
        if (OperadorIsOnline(owner.user.username, operador.usuario_vicloning) == 'on'):
            operatorsOnline = True
            # print '!!!',operador.usuario_vicloning.id,"online"
        else:
            # print '??',operador.usuario_vicloning.id,"offline"
            operador.status = 'off'
            operador.save()
            # pass

    chat_online, hasMail = usuario_online(
            sesion, owner, email, url, chat=chat, name=name, lang=lang, userLost=userLost)

    # print 'userLost',userLost
    return HttpResponse(
        json.dumps({'operatorsOnline': operatorsOnline, 'chat_online': chat_online, 'hasMail': hasMail}),
        content_type="application/json")


# @cache_jquery
@login_required(login_url=reverse_lazy("login_extensions"))
# @checksteps()
def extensions_notify(request):
    return render_to_response('notify_extension_page.html', RequestContext(request))


@login_required(login_url=reverse_lazy("login"))
# @checksteps()
def etiquetas(request, interno=''):
    owner = get_padre(request.user.usuario_vicloning)
    etiquetas = {}
    tag_num = {}
    lista = []
    infos = Info.objects.filter(owner=owner).exclude(tags='')
    # print 'tags:',infos.count()
    for info in infos:
        lista_tags = info.tags.split(',')
        for tag in lista_tags:
            tag = tag.strip()
            if tag != '':
                if tag in etiquetas.keys():
                    etiquetas[tag].append(info.id)  ###info.email
                else:
                    etiquetas[tag] = [info.id]  # .email
                tag_num[tag] = len(etiquetas[tag])

    for key, value in tag_num.items():
        lista.append({'tag': key, 'num': value})

    if interno:
        return lista

    return HttpResponse(json.dumps(lista), content_type="application/json")


def change_operador(request):
    # JQUERY
    params = request.POST
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    id_user = info_cambiado(params['id'])
    cambio_op = etiqueta(etiqueta_cambio, encriptar_user(owner.id))
    cambios(cambio_op, str(datetime.datetime.now()))
    info = Info.objects.get(id=id_user, owner=owner)
    defaults = {}
    sala = info.sala_asignada

    if params['operador'] == 'bot':
        sala.operador = ''
        sala.save()
        infoInChat = etiqueta(etiqueta_chat + marketeer, info.id)
        chat_activo = cache.set(infoInChat, 'go_to_offline', timeout=time_online)
        return HttpResponse(json.dumps({'result': 'ok'}), content_type="application/json")

    elif sala.operador != params['operador']:
        sala.operador = params['operador']
        sala.save()
        Dialogos.objects.filter(room=sala, operador='').update(operador=sala.operador)

    info.save()

    ##mensaje interno entre agentes
    if 'mensaje' in params.keys():
        mensaje = params['mensaje']
        data = {'email': info.email, 'name': info.nombre,
                'internalMessage': params['mensaje'], }
        send_email_template('Notifications - No Reply <noreply@marketeer.co>', UsuarioVicloning.objects.get(
                id=sala.operador), 'Important: New Conversation assigned to you!', 'mail_form_newAssigned.html', data)
        if mensaje != '':
            Dialogos(room=sala, author='CHAT_MSG_INTERNAL', content=mensaje,
                     operador=request.user.usuario_vicloning.id).save()

    return HttpResponse(json.dumps({'result': 'ok'}), content_type="application/json")


def default(self):
    return ''


@csrf_exempt
def login(request, id, marketeer):
    usuario = desencriptar_user(marketeer)  # id de UsuarioVicloning
    owner = UsuarioVicloning.objects.get(id=usuario)
    sesion = request.COOKIES.get('mktr' + str(usuario))
    nocookie = request.COOKIES.get('nocookie' + str(usuario))
    if sesion == nocookie:
        return HttpResponse(json.dumps(({'status': 'Cookies Disabled'})), content_type="application/json")
    try:
        info = Info.objects.get(id=id, owner=owner)
    except:
        return HttpResponse(json.dumps(({'status': 'User do not exist'})), content_type="application/json")
    chat_online, hasMail = usuario_online(sesion, owner, info.email, name=info.nombre)
    return HttpResponse(json.dumps(({'status': 'User login'})), content_type="application/json")


@csrf_exempt
@cache_angular
def chat(request):
    # ------------------------------------
    # Desencripta el id de usuario:

    params = json.loads(request.POST.keys()[0])

    if 'leido' in params.keys() and leido == True:
        leido = True
    else:
        leido = False

    marketeer, idioma, isLoaded = params[
                                      'marketeer'], params['idioma'], params['isLoaded']

    usuario = desencriptar_user(marketeer)  # id de UsuarioVicloning
    owner = UsuarioVicloning.objects.get(id=usuario)
    marketeer = str(owner.user)
    sesion = request.COOKIES.get('mktr' + str(usuario))
    nocookie = request.COOKIES.get('nocookie' + str(usuario))
    if sesion == nocookie:
        return HttpResponse(json.dumps(({'status': 'Cookies Disabled'})), content_type="application/json")
    ip = get_ip(request)
    title_url, url = get_tittle_url(params)

    info = Info.objects.filter(owner=owner).get(
            id=int(whoIam(sesion, marketeer)))

    defaults = {'idioma': params['idioma']}
    sala = info.sala_asignada
    if 'rate' in params.keys() and params['rate'] != '' and params['rate'] != 'none':
        try:
            sala.rate = int(params['rate'])
        except:
            sala.rate = None
    sala.idioma = idioma
    sala.status = 'void'
    sala.save()

    # Genera el mensaje a guardar
    mensaje = html_to_text(params['mensaje'])
    # dialogo=Dialogos(room=sala,author='CHAT_MSG_AUTHOR_USER',content=mensaje,url=url
    ip = get_client_ip(request)
    # ip='5.63.144.156' #falta quitar test
    # ip='81.60.139.138' #falta


    info.idioma = idioma

    ua_string = request.META.get('HTTP_USER_AGENT', '')

    user_agent = parse(ua_string)
    visitas = Visita.objects.filter(
            owner=owner, room=sala).order_by('-created')
    city = ''
    country = ''
    if visitas:
        ip_anterior = visitas[0].ip
    else:
        ip_anterior = ''

    # buscamos datos del usuario por whois y por geoip
    if ip != ip_anterior:
        g = GeoIP()
        """try:
                                    data_geoip = g.city(ip)
                                    try:
                                        city = data_geoip['city']
                                    except:
                                        city = ''
                                    try:
                                        country = data_geoip['country_name']
                                    except:
                                        country = ''
                                except:
                                    pass"""

        # try:
        #    whois = IPWhois(ip).lookup(False)
        # except:
        whois = {}
        if whois == {}:
            g = GeoIP()
            try:
                data_geoip = g.city(ip)
                try:
                    city = data_geoip['city']
                except:
                    city = ''
                try:
                    country = data_geoip['country_name']
                except:
                    country = ''
            except:
                pass

        else:
            try:
                city = whois['nets'][0]['city']
            except:
                city = ''
            try:
                countries = get_countries()
                country = countries[whois['nets'][0]['country']]
            except:
                country = ''
            try:
                whois['nets'][0]['description']
            except:
                pass
            try:
                direccion = whois['nets'][0]['address']
                if info.direccion == '':
                    info.direccion = direccion
                    info.save()
            except:
                pass
            try:
                company = whois['nets'][0]['name']
                if info.company == '':
                    info.company = company
                    info.save()
            except:
                pass
    if city == None or city == 'null':
        city = ''

    defaults = {'ip': ip, 'pais': country}

    sesion_obj, created = Sesion.objects.get_or_create(
            codigo=sesion, usuario_vicloning=owner, defaults=defaults)

    if mensaje != '':
        # cuidamos que no metan html en el chat para hacer exploit
        mensaje = html_to_text(mensaje)
        dialogo = Dialogos(
                url=url, room=sala, operador=sala.operador, author='CHAT_MSG_AUTHOR_USER', content=mensaje, ip=ip,
                country=country,
                city=city, browser=user_agent.browser.family, sesion_user=sesion, device=user_agent.device.family)

        dialogo.save()
        # Marcamos Contacto            ------------------------------##
        try:
            referrer = request.META['HTTP_REFERER']
        except:
            referrer = ''
        info.mensajes_sin_leer = info.mensajes_sin_leer + 1

        marca_visita(
                owner, info, 'chat_recibido', referrer, sesion, dialogo)
        # ----------------------------------------------------------##

    info.borrado = False
    info.save()
    if info.sala_asignada.operador:
        alert_operator(usuario, info.sala_asignada.operador, ' New Visit: ' + info.nombre, sesion)
    else:
        if info.nombre:
            operadores = OperatorConnection.objects.filter(status='on', usuario_vicloning__in=(
            UsuarioVicloning.objects.filter(Q(padre=owner) | Q(user=owner.user))))
            for operador in operadores:
                alert_operator(usuario, operador.usuario_vicloning, ' New Visit: ' + info.nombre, sesion)
    time = params['time']
    url_key1 = "<owner_{0}>-HISTORICO-{1}".format(sala.owner.id, sala.id)
    url_key2 = "<owner_{0}>-HISTORICO-{1}".format(sala.owner.id, sesion)
    chat1 = cache.get(key=url_key1, default=[])
    chat2 = cache.get(key=url_key2, default=[])
    if len(chat1) > len(chat2):
        chat = chat1
    else:
        chat = chat2
    usuarioSinChat = False  # esto es para saber si el usuario ha chateado o no
    if len(chat) == 0:
        usuarioSinChat = True
    cache.set(key=url_key1, value=chat, timeout=60)
    cache.set(key=url_key2, value=chat, timeout=300)
    chat_final = []
    chat_completo = []
    time_chat = str(datetime.datetime.now())
    list_url_key = "<owner_{0}>-URLS-{1}-{2}".format(
            sala.owner.id, info.id, url)

    # marcamos la visita
    try:
        if not cache.get(list_url_key):
            visitas = Visita.objects.filter(
                    owner=owner, room=sala).order_by('-created')
            if visitas:
                visita = visitas[0]
                if visita.url != url:
                    vis = Visita(
                            owner=owner, room=sala, idioma=idioma, url=url, ip=ip, country=country, city=city,
                            title=title_url,
                            browser=user_agent.browser.family, sesion_user=sesion, device=user_agent.device.family)
                    vis.save()
            else:
                vis = Visita(
                        owner=owner, room=sala, idioma=idioma, url=url, ip=ip, country=country, city=city,
                        title=title_url,
                        browser=user_agent.browser.family, sesion_user=sesion, device=user_agent.device.family)
                vis.save()
            cache.set(list_url_key, True)
        else:
            cache.set(list_url_key, cache.get(list_url_key), 300)
    except:
        pass

    # miramos si tiene operador asignado
    operator = ''
    operatorMail = ''
    operator_assigned = ''
    for item in chat:
        if 'fecha' in item.keys() and str(item['fecha']):
            # if str(item['fecha'])>str(time):
            #    chat_final.append(item)
            chat_final.append(item)
            try:
                operator_assigned = UsuarioVicloning.objects.filter(id=sala.operador)
                if operator_assigned:
                    operator_assigned = operator_assigned[0]
                    operator = {
                        "img": str(operator_assigned.avatar),
                        "nombre": operator_assigned.user.first_name + ' ' + operator_assigned.user.last_name
                    }
                    operatorMail = operator_assigned.user.email
            except:
                pass

    # ESTADO OPERADORES para mandar mails! Miramos si hay operadores conectados para mandar mail
    if mensaje != '':
        mail_if_not_online(operator_assigned, info, dialogo)

    # puedo agregar al JSON 'estado_operadores':dataOperadores,
    # comprobando operadores END#####

    # chat_completo return
    # HttpResponse(json.dumps(({'operador':operator,'chat':(chat_final),'chat_completo':chat_completo,'Time':str(time_chat)})),
    # content_type="application/json")

    # if isTyping(info.id,sala.operador) != params['typing']:
    #    cambio_op = etiqueta(etiqueta_cambio, encriptar_user(usuario))
    #    cambios(cambio_op, str(datetime.datetime.now()))

    if 'typing' in params.keys():
        typing(info.id, sala.operador, params['typing'])
    else:
        typing(info.id, sala.operador, False)

    return HttpResponse(json.dumps(({'typing': isTyping(sala.operador, info.id),
                                     'userNoChat': usuarioSinChat, 'rate': sala.rate, 'operador': operator,
                                     'chat': (chat_final), 'Time': str(time_chat)})), content_type="application/json")


def mail_if_not_online(operator_assigned, info, dialogo):
    # nos aseguramos que no hay operadores conectados
    owner = dialogo.room.owner
    marketeer = owner.user.username
    hayoperatorsOnline = False
    operadores_u = UsuarioVicloning.objects.filter(Q(padre=owner) | Q(id=owner.id))
    operadores = OperatorConnection.objects.filter(usuario_vicloning__in=operadores_u)
    dataOperadores = []
    mail_owner = False
    subject = "You have a new message! From your Website"
    envia = str(info.email)

    mediaUrlTest = '/media/' if 'media/' not in str(owner.assistant.avatar) else ''
    data = {
        # FALTA NOMBRE DEL QUE ENVIA que GRABE en info!!!
        'mensaje': dialogo.content,
        'leadmail': envia,
        'senderName': 'Your Marketeer',  # el avatar de marketeer
        'avatar': settings.HTTPS_URL + mediaUrlTest + str(owner.assistant.avatar),
        'URLtoReply': settings.HTTPS_URL,  # FALTA
        'urlserver': settings.HTTPS_URL,
        'id_dialogo': dialogo.id,
        'company': str(owner.company),
    }
    operadores_online = operadores.filter(status='on')
    # si hay operador asignado pero el no esta online, le mandamos mail
    enviar_mail_a_todos = False
    hayoperatorsOnline = True if operadores_online else False

    if operator_assigned:
        print
        ' ASIGNADO '
        operatorMail = operator_assigned.user.email
        if OperatorConnection.objects.get(usuario_vicloning=operator_assigned).status == 'off':
            try:
                send_email_template_operator(
                        envia, str(operator_assigned.usuario_vicloning.mail_zendesk), subject,
                        'mail_chat_agentOffline.html', data)
            except:
                send_email_template_operator(
                        envia, operatorMail, subject, 'mail_chat_agentOffline.html', data)
    elif not operadores_online:
        print
        ' ENVIA A TODOS '
        enviar_mail_a_todos = True
        try:
            send_email_template_operator(
                    envia, str(owner.mail_zendesk), subject, 'mail_chat_agentOffline.html', data)
        except:
            pass
    # hacemos la lista de operadores
    for operador in operadores:
        dataOperadores.append({
            'id': operador.usuario_vicloning.id,
            'nombre': operador.usuario_vicloning.user.first_name + ' ' + operador.usuario_vicloning.user.last_name,
            'status': OperadorIsOnline(marketeer, operador.usuario_vicloning),
            'avatar': str(operador.usuario_vicloning.avatar)
        })
        # if operador.status == 'off' and ((not len(user) and not operadores_online) or operador.usuario_vicloning == user):
        if enviar_mail_a_todos:
            try:
                # intenta mandar mail a todos los operadores
                try:
                    send_email_template_operator(
                            envia, str(operador.usuario_vicloning.mail_zendesk), subject, 'mail_chat_agentOffline.html',
                            data)
                except:
                    send_email_template_operator(
                            envia, operador.user.email, subject, 'mail_chat_agentOffline.html', data)
            except:
                pass


@login_required(login_url=reverse_lazy("login"))
@checksteps()
def archiva(request):
    # JQUERY
    params = request.POST
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    sala = Info.objects.get(owner=owner, id=params['id']).sala_asignada
    sala.status = 'closed'
    sala.save()
    return HttpResponse(json.dumps({'archived': params['id']}), content_type="application/json")


@login_required(login_url=reverse_lazy("login"))
@checksteps()
def elimina(request):
    # JQUERY
    params = request.POST
    # email = params['email']
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    sala = Info.objects.get(owner=owner, id=params['id']).sala_asignada
    sala.status = 'deleted'
    sala.save()
    return HttpResponse(json.dumps({'deleted': params['id']}), content_type="application/json")


@login_required(login_url=reverse_lazy("login"))
@checksteps()
def change_lead(request):
    # JQUERY

    params = request.POST
    ret = {}
    try:
        operador, owner, marketeer = get_operador_owner_marketeer(request)

        lead = Info.objects.get(id=params['id'], owner=owner)
        for param, value in params.items():
            setattr(lead, param, value)
            lead.save()
            ret[param] = value

        cambio_op = etiqueta(etiqueta_cambio, encriptar_user(owner.id))
        cambios(cambio_op, str(datetime.datetime.now()))
    except:
        ret = {'error': 'error'}

    return HttpResponse(json.dumps(ret), content_type="application/json")


@login_required(login_url=reverse_lazy("login"))
@checksteps()
def bloquea(request):
    # JQUERY
    params = request.POST
    email = params['email']
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    # buscamos el usuario padre
    sala = Info.objects.get(owner=owner, id=params['id']).sala_asignada
    sala.status = 'blocked'
    sala.save()
    return HttpResponse(json.dumps({'deleted': params['id']}), content_type="application/json")


def desbloquea(request):
    # JQUERY
    params = request.POST
    email = params['email']
    operador, owner, marketeer = get_operador_owner_marketeer(request)
    # buscamos el usuario padre
    sala = Info.objects.get(owner=owner, id=params['id']).sala_asignada
    sala.status = 'void'
    sala.save()
    return HttpResponse(json.dumps({'deleted': params['id']}), content_type="application/json")


def replaceTags(mensaje, info):
    # print mensaje
    mensaje = mensaje.replace('%%NAME%%', info.nombre)
    mensaje = mensaje.replace('%%LAST_NAME%%', info.last_name)
    mensaje = mensaje.replace('%%COMPANY%%', info.company)
    # print mensaje
    return mensaje


@login_required(login_url=reverse_lazy("login"))
@checksteps()
@cache_jquery
def chat_operador(request):
    # ------------------------------------
    # Desencripta el id de usuario:

    params = request.POST
    email = params.get('mail', '')
    id_user = info_cambiado(params.get('id', ''))

    # usuario = owner = request.user.usuario_vicloning
    owner = request.usuario_vicloning
    usuario = request.usuario_vicloning
    while owner.padre:
        owner = owner.padre

    if not email and params.get('id', '') != '':
        info0 = Info.objects.get(id=params.get('id', ''), owner=owner)
        email = info0.additional_email if info0.additional_email != '' else info0.email

    defaults = {'nombre': 'Visitor'}
    viae = False
    viaw = False
    if 'via' in params.keys():
        if params['via'] == 'e':
            viae = True
        if params['via'] == 'w':
            viaw = True
    else:
        viae = True
        viaw = True
    lista_online = whoIsOnline(owner.user.username)
    # buscamos el usuario padre

    # si es una sesion - usuario desconocido - no logueada en live chat
    """algo falla aqui
        if '@' not in email:
                    try:
                        email=mailFromSession(owner.user.username,request.COOKIES.get('mktr'+str(padre.id)))
                    except:
                        email=email"""
    try:
        info = Info.objects.get(owner=owner, id=id_user)
    except:
        if email:
            try:
                info = Info.objects.get(owner=owner, email=email)
            except:
                # el info se ha borrado de visitor a un email al registrarse por lo que no devuelve info
                return HttpResponse({'data': 'no info'}, content_type="application/json")
        else:
            return HttpResponse({'data': 'no info'}, content_type="application/json")
    if info.sala_asignada != None:
        sala = info.sala_asignada

    else:
        sala = Sala(owner=owner, user_email=info.email, user_name=info.nombre)
        sala.save()
        info.sala_asignada = sala
        info.save()

    if 'typing' in params.keys():
        tp = True if params['typing'] == 'true' else False
        typing(sala.operador, info.id, tp)
    else:
        typing(sala.operador, info.id, False)

    sesion = email if email != '' else ''
    idioma = sala.idioma
    sesion = sala.user_email

    # Genera el mensaje a guardar
    mensaje = params['mensaje'] if 'mensaje' in params.keys() else ''
    if 'id_template' in params.keys():
        if params['id_template'] != '':
            template = AM_EmailTemplates.objects.get(owner=owner, id=int(params['id_template']))
            if info in template.infos_sent.all():
                mensaje = ''
    if mensaje != '':
        # mensaje=mensaje.replace('src="//','src="https://')
        # mensaje=mensaje.replace('href="/','href="https://marketeer.co/')
        mensaje = mensaje.replace('href="//', 'href="https://')
        mensaje = mensaje.replace('href="/', 'href="https://my.marketeer.co/')
        mensaje = mensaje.replace('src="//', 'src="https://')
        mensaje = mensaje.replace('src="/', 'src="https://my.marketeer.co/')
        mensaje = replaceTags(mensaje, info)
        soup = BS(mensaje)
        for imgtag in soup.find_all('img'):
            try:
                if 'data:image/jpeg;base64' in imgtag["src"] or 'data:image/png;base64' in imgtag["src"]:
                    if not os.path.exists(settings.MEDIA_ROOT + '/' + settings.CLIENT_UPLOADS_DIR + '/' + str(
                            owner.id)):
                        os.makedirs(settings.MEDIA_ROOT + '/' + settings.CLIENT_UPLOADS_DIR + '/' + str(owner.id))
                    name = b64encoded_img2file(imgtag["src"],
                                               settings.MEDIA_ROOT + '/' + settings.CLIENT_UPLOADS_DIR + '/' + str(
                                                   owner.id) + '/', new_width=700)
                    imgtag['src'] = settings.HTTPS_URL + settings.MEDIA_URL + settings.CLIENT_UPLOADS_DIR + '/' + str(
                        owner.id) + '/' + name
                    #
            except:
                pass

                # imgtag["src"]

        mensaje = unicode(soup)

        if sala.operador == '':
            sala.operador = usuario.id
        dialogo = Dialogos(room=sala, author='CHAT_MSG_AUTHOR_OPERATOR',
                           content=mensaje, operador=usuario.id)
        if 'id_template' in params.keys():
            if params['id_template'] != '':
                dialogo.template = params['id_template']
                template = AM_EmailTemplates.objects.get(owner=owner, id=int(params['id_template']))
                template.infos_sent.add(info)
                template.save()
                # ip=ip,country=g.country(ip),city='g.city(ip)',browser=user_agent.browser.family)#,device=user_agent.device.family)
        dialogo.save()

        if 'learning' in params.keys() and params['learning'] == 'true':
            create_faq_dialogo(dialogo)

        if sala.status != 'blocked':
            sala.status = 'void'
            sala.save()

    time = params['time']  # tiempo de la ultima peticion
    # numero de visitas
    visits = Visita.objects.filter(room=sala).order_by('created')
    sesiones_user = [visita.sesion_user for visita in visits]
    chats_objects = Dialogos.objects.filter(room=sala)
    chat_objects = chats_objects.order_by('created')
    for chato in chat_objects:
        # print 'chato.content', chato.content
        try:
            if chato.content != html_to_text(
                    chato.content) and chato.author == 'CHAT_MSG_AUTHOR_USER' and chato.browser != 'Email':
                chato.content = html_to_text(str(chato.content))
                chato.save()
        except:
            pass
    # payload = {"recipient":{"id":info.facebookid},"message":{"text":html2text.html2text(mensaje)}}
    # r = requests.post(url, headers={"content-type":"application/json"}, data=json.dumps(payload))

    chat = serializers.serialize('json', chat_objects)
    visitas = serializers.serialize(
            'json', Visita.objects.filter(room=sala).order_by('created'))
    preguntas_user = serializers.serialize(
            'json', PreguntaUsuario.objects.filter(sesion__codigo__in=sesiones_user))
    search_user = serializers.serialize(
            'json', SearchUsuario.objects.filter(sesion__codigo__in=sesiones_user))
    info_to_append = {}
    info_to_append['email'] = sala.user_email
    info_to_append['returning'] = 0 < Dialogos.objects.filter(
            room=sala, author='CHAT_MSG_AUTHOR_USER',
            created__lte=(datetime.datetime.now() - datetime.timedelta(hours=24))).count()

    # buscamos el lead de la sala clicada
    if str(sala.operador) == str(usuario.id):
        info.mensajes_sin_leer = 0
        info.save()

    try:
        ultimo_mensaje = info.sala_asignada.ultimo_mensaje  # Dialogos.objects.filter(room=sala, author='CHAT_MSG_AUTHOR_USER').order_by('-created')[0]
        info_to_append['mensaje'] = ultimo_mensaje.content[:20]
        info_to_append['time_to_sort'] = str(ultimo_mensaje.created)
        if (datetime.datetime.now().strftime("%d/%m/%y") == ultimo_mensaje.created.strftime("%d/%m/%y")):
            info_to_append[
                'time'] = ultimo_mensaje.created.strftime("%H:%M:%S")
        else:
            info_to_append['time'] = ultimo_mensaje.created.strftime(
                    "%d/%m/%y (%H:%M:%S)")
    except:
        pass
    try:

        visita = visits.order_by('-created')[0]
        info_to_append['URL'] = visita.url
        info_to_append['titleURL'] = visita.title
        info_to_append['browser'] = visita.browser
        info_to_append['ip'] = visita.ip
        info_to_append['city'] = visita.city if visita.city else info.city
        info_to_append['country'] = visita.country if visita.country else dict(countries)[info.country.code]
        # list_url_key ="<owner_{0}>-URLS-{1}-{2}".format(sala.owner.id, sala.user_email,url)

    except:
        if info.nombre == defaults['nombre'] and info.email not in lista_online:
            # info.delete()
            # sala.delete()
            pass

        info_to_append['mensaje'] = ''
        info_to_append['time_to_sort'] = ''
        info_to_append['time'] = ''
        info_to_append['ip'] = ''
        info_to_append['city'] = info.city
        if info.country.code in dict(countries):
            info_to_append['country'] = dict(countries)[info.country.code]
    info_to_append['archivado'] = sala.status == 'closed'
    info_to_append['eliminado'] = sala.status == 'deleted'
    info_to_append['contact_id'] = info.id
    info_to_append['tags'] = info.tags
    info_to_append['operador'] = sala.operador
    info_to_append['avatar'] = info.avatar
    info_to_append['status'] = info.status
    info_to_append['estado'] = info.status
    info_to_append['prioridad'] = info.priority
    info_to_append['facebook'] = info.facebookid != ''

    for dialogoaux in chat_objects:
        dialogoaux.new = False
        # if dialogoaux.author=='CHAT_MSG_AUTHOR_OPERATOR':
        # create_faq_dialogo(dialogoaux)
        dialogoaux.save()
    if str(time) != '0':
        chat = serializers.serialize(
                'json', chats_objects.filter(created__gt=time).order_by(
                    'created'))  # Dialogos.objects.filter(room=sala, created__gt=time).order_by('created'))
        visitas = serializers.serialize(
                'json', Visita.objects.filter(room=sala, created__gt=time).order_by('created'))
        preguntas_user = serializers.serialize(
                'json', PreguntaUsuario.objects.filter(sesion__codigo__in=sesiones_user, fecha__gt=time))
        search_user = serializers.serialize(
                'json', SearchUsuario.objects.filter(sesion__codigo__in=sesiones_user, fecha__gt=time))
    faqs_user_ = serializers.serialize('json', FaqListada.objects.filter(
            pregunta__in=[preg['pk'] for preg in json.loads(preguntas_user)]))
    faqs_user = []
    for faq in json.loads(faqs_user_):
        try:
            FaqClicada.objects.get(faq_listada__id=faq['pk'])
            faq['clickada'] = True
        except:
            faq['clickada'] = False
        faqs_user.append(faq)
    chat_final = []
    chat_completo = []
    elementos = json.loads(chat)
    elementos.extend(json.loads(visitas))
    elementos.extend(json.loads(preguntas_user))
    elementos.extend(json.loads(search_user))
    elementos.extend((faqs_user))

    element = sorted(elementos, key=lambda k: str(
            k['fields']['created'] if 'created' in k['fields'] else k['fields']['fecha']), reverse=False)

    for item in element:
        if 'created' in item['fields'].keys():
            files = UserFile.objects.filter(dialogo__id=item['pk'])
            item['attachments'] = []
            if 'content' in item['fields'].keys():
                try:
                    item['fields']['content'] = item['fields']['content'].replace('<blockquote',
                                                                                  '<div class="hideQuotes btn btn-info btn-sm" onclick="$(this).parent().find(\'blockquote\').toggle()" title="See Original Message">...</div><blockquote')
                except:
                    pass
            for userfile in files:
                if userfile.upload:
                    try:
                        item['attachments'].append({"name": userfile.upload.name, 'link': userfile.upload.url})
                    except:
                        item['attachments'][{"name": userfile.upload.name, 'link': userfile.upload.url}]
            msg_time = parser.parse(item['fields']['created'])
            if (datetime.datetime.now().strftime("%d/%m/%y") == msg_time.strftime("%d/%m/%y")):
                item['timesince'] = msg_time.strftime("%H:%M:%S")
            else:
                item['timesince'] = msg_time.strftime("%d/%m/%y (%H:%M:%S)")
            if str(item['fields']['created']) > str(time):
                chat_final.append(item)
            chat_completo.append(item)

        if 'fecha' in item['fields'].keys():
            msg_time = parser.parse(item['fields']['fecha'])
            if (datetime.datetime.now().strftime("%d/%m/%y") == msg_time.strftime("%d/%m/%y")):
                item['timesince'] = msg_time.strftime("%H:%M:%S")
            else:
                item['timesince'] = msg_time.strftime("%d/%m/%y (%H:%M:%S)")
            if ' '.join(str(item['fields']['fecha']).split('T')) > str(time):
                chat_final.append(item)
            chat_completo.append(item)

    time_chat = str(datetime.datetime.now())

    # si pongo owner.user.username veo todos los usuarios
    nombre_padre = owner.user.username
    # hay que ver todos cuando no estan asigandos, sino listar a asignados a
    # otros
    # sesion en vez de email para asegurar que comprueba
    # userIsOnline = isOnline(str(sesion), nombre_padre)
    userIsOnline = isOnline(str(id_user), nombre_padre)

    trySendEmail = ''  # para envio de mail, si da error debe comunicarlo

    telefono = info.phone

    # MANDA WHATSAPP
    if (not userIsOnline) and mensaje != '' and telefono != '' and viaw:

        envia_whatsapp(owner, 'mensaje', telefono, mensaje, dialogo.id)
        # Marcamos Contacto            ------------------------------##
        try:
            referrer = request.META['HTTP_REFERER']
        except:
            referrer = ''
        marca_visita(owner, info, 'whatsapp_enviado', referrer, '', dialogo)
        # ----------------------------------------------------------##

    if (info.facebookid != '' and mensaje != ''):
        envia_facebook(mensaje, owner, info, dialogo)

    # envia mail al usuario si esta desconectado
    if not userIsOnline and mensaje != '' and email != '' and viae:
        operador = usuario
        subject = subject0 = "You have a new message! From " + \
                             operador.user.get_full_name()
        if 'subject' in params.keys():
            # print 'subject'
            if params['subject'] != '':
                subject = params['subject']
            subject = replaceTags(subject, info)
        if operador.assistant.activated_marketeer_mail:
            envia = str(operador.user.first_name) + ' ' + str(operador.user.last_name) + ' <' + str(
                operador.user.username + '@app.marketeer.co') + '>'
        else:
            envia = str(operador.user.first_name) + ' ' + str(operador.user.last_name) + ' <' + str(
                operador.mail_zendesk) + '>'
        # envia = u'support@marketeer.co'
        website = ''
        try:
            website = str(owner.primary_website)
        except:
            pass
        data = {
            'mensaje': mensaje,
            'senderName': operador.user.first_name,
            'avatar': settings.HTTPS_URL + str(operador.avatar),
            'URLtoReply': website,  # FALTA
            'urlserver': settings.HTTPS_URL,
            'leadmail': ", ".join([info.email, info.additional_email]),
            'email': info.email,
            'id_dialogo': dialogo.id,
            'company': owner.company,
            'name': info.nombre,
            'auto_mail_activated': operador.assistant.activated_marketeer_mail
        }

        # miro si el mail existe y si es de verdad
        mail_principal = info.email if '@' in info.email and '.' in info.email else ''
        mail_secundario = info.additional_email if '@' in info.additional_email and '.' in info.additional_email and mail_principal != info.additional_email else ''
        mail_a_mandar = ''
        # mirando a que mail le mando, primero tengo que saber si tiene alguno
        if mail_principal and mail_secundario:
            mail_a_mandar = [mail_principal, mail_secundario]
        elif mail_principal or mail_secundario:
            mail_a_mandar = mail_principal if mail_principal else mail_secundario
        try:
            referrer = request.META['HTTP_REFERER']
        except:
            referrer = ''

        if mail_a_mandar:
            # print '22'
            trySendEmail = send_email_template_user(
                    envia, mail_a_mandar, subject, 'mail_chat_userOffline.html', data)
            # print '23'
            if trySendEmail == True:
                marca_visita(owner, info, 'email_enviado', referrer, '', dialogo)
                # print '24'
        else:
            trySendEmail = 'Error'
            # dialogo.delete() <- no va porque se carga todas las referencias de dialogo y acaba borrando el contacto

    # user_is_online : usuario si esta online es true
    # isChatOnline : pregunta si hay alguien online
    # user_is_in_chat : si el usuario esta en modo chat o en modo automatico
    respuesta_http = json.dumps(
            ({'trySendEmail': trySendEmail, 'PRIORITY_CHOICES': PRIORITY_CHOICES, 'STATUS_CHOICES': STATUS_CHOICES,
              'user_is_in_chat': isUserInChat(info.id, owner), 'extra': info_to_append, 'operador': sala.operador,
              'status_user': userIsOnline, 'lang': idioma, 'chat': chat_final, 'time': time_chat,
              'typing': isTyping(info.id, sala.operador)}))
    return HttpResponse(respuesta_http, content_type="application/json")


def envia_facebook(mensaje, owner, info, dialogo, sugeridas=[]):
    # COMUN:

    fb_page = info.FBpage
    recipient_id = info.facebookid
    url = 'https://graph.facebook.com/v2.6/me/messages?access_token=' + fb_page.access_token
    mensaje = mensaje.replace('href="//', 'href="https://')
    mensaje = mensaje.replace('href="/', 'href="https://my.marketeer.co/')
    mensaje = mensaje.replace('src="//', 'src="https://')
    mensaje = mensaje.replace('src="/', 'src="https://my.marketeer.co/')

    # mensaje=mensaje.replace('&gt;','>')
    """
    try:
        mensaje=mensaje.decode('utf-8')
        mensaje=mensaje.replace(u'&gt;',u'>')
    except:
        pass
    html=mensaje

    #Si tiene mensaje de texto:
    text_maker = html2text.HTML2Text()
    text_maker.protect_links=True
    #text_maker.ignore_links = True
    text_maker.links_each_paragraph=True
    text_maker.ignore_emphasis=True
    text_maker.re_unescape=True
    mensajes=[]
    soup = BS(html)
    dict_img={}
    dict_url={}

    except:
        pass
        #[How do i change the text of the bubble](<javascript:scope.askMarketeer\\('How do i change the text of the bubble'\\)>)
        #[How do i change the text of the bubble](<javascript:scope.askMarketeer\\('How do i change the text of the bubble'\\)>)
    for l in text_maker.handle(html).split('\n'):
        #l=text_maker.handle(html)
        if l!='':
            #printed=False
            for image in dict_img.keys():
                if image in l:
                    mensajes.append({'text':l.split(image)[0]})
                    mensajes.append({
                        "attachment":
                            {
                            "type":"image",
                            "payload":{"url":dict_img[image]}
                            }
                        })
                    mensajes.append({'text':l.split(image)[1]})
                    #printed=True
            if not #printed:
                mensajes.append({'text':l})

    for trozo in mensajes:
        if 'text' in trozo.keys() and u'/StartChat' in trozo['text']:
            trozo={
                        "attachment":
                            {
                            "type":"template",
                            "payload":{
                                "template_type":'button',
                                "text":trozo['text'].replace(u'/StartChat',u''),
                                "buttons":[{
                                                                    "type":"postback",
                                                                    "title":"Start CHAT",
                                                                    "payload":'/startchat',
                                                                    },]
                                }
                            }
                        }
        for link in dict_url.keys():
            #print link
            if 'text' in trozo.keys() and link in trozo['text']:
                if dict_url[link][0]=='':
                    trozo['text']=trozo['text'].replace(link,dict_url[link][1])
                elif 'http' in dict_url[link][0]:
                    trozo={
                            "attachment":
                                {
                                "type":"template",
                                "payload":{
                                    "template_type":'button',
                                    "text":trozo['text'].replace(link,dict_url[link][1]),
                                    "buttons":[{
                                                "type":"web_url",
                                                "title":dict_url[link][1],
                                                "url":dict_url[link][0],
                                                },]
                                    }
                                }
                            }
                else:
                    trozo={
                        "attachment":
                            {
                            "type":"template",
                            "payload":{
                                "template_type":'button',
                                "text":trozo['text'].replace(link,dict_url[link][1]),
                                "buttons":[{
                                                                    "type":"postback",
                                                                    "title":dict_url[link][1],
                                                                    "payload":dict_url[link][0],
                                                                    },]
                                }
                            }
                        }
            elif 'attachment' in trozo.keys() and 'text' in trozo['attachment']['payload'].keys() and link in trozo['attachment']['payload']['text']:
                if dict_url[link][0]=='':
                    trozo['attachment']['payload']['text']=trozo['attachment']['payload']['text'].replace(link,dict_url[link][1])
                elif 'http' in dict_url[link][0]:
                    trozo['attachment']['payload']['text']=trozo['attachment']['payload']['text'].replace(link,dict_url[link][1])
                    trozo['attachment']['payload']['buttons'].append({"type":"web_url","title":dict_url[link][1],"url":dict_url[link][0],})
                else:
                    trozo['attachment']['payload']['text']=trozo['attachment']['payload']['text'].replace(link,dict_url[link][1])
                    trozo['attachment']['payload']['buttons'].append({"type":"postback","title":dict_url[link][1],"payload":dict_url[link][0]})
        """

    texto = html_to_text(mensaje).encode('utf-8').strip().replace('\n \n', '\n')

    soup = BS(mensaje, 'html.parser')

    txt2 = []
    t1 = ''
    for s in soup.find_all('a', href=True):
        if 'javascript:' in s['href'] or 'mailto:' in s['href']:
            s.extract()
        else:
            urlShorter = Urls(url=s['href'], info=info)
            urlShorter.save()
            s["href"] = urlShorter.shorten_url()

    longitud = len(texto)
    txt = []
    tope, ini = 0, 0
    n = 0
    while (ini < longitud) and n < 50:
        n = n + 1
        tope = ini + 300
        ini2 = texto.rfind("\n", ini, tope)
        if ini2 == -1 or ini2 == ini:
            ini2 = tope
        txt.append(texto[ini:ini2])
        ini = ini2

    mensajes = []
    buttons = []
    for imgtag in soup.find_all('img'):
        try:
            mensajes.append({
                "attachment":
                    {
                        "type": "image",
                        "payload": {"url": imgtag['src']}
                    }
            })
        except:
            pass

    for link in soup.findAll('a', href=True):
        buttons.append({
            "type": "web_url",
            "title": html_to_text(link.contents[0].encode('utf-8')).capitalize(),
            "url": link['href'],
        })

    for sugerida in sugeridas:
        buttons.append({
            "type": "postback",
            "title": sugerida['pregunta'],
            "payload": sugerida['pregunta'],
        })

    for trozo in txt:
        mensajes.append({'text': trozo})
    if len(buttons) > 0:
        mensajes[-1] = {"attachment": {
            "type": "template",
            "payload": {
                "template_type": 'button',
                "text": mensajes[-1]['text'],
                "buttons": buttons[:3]
            }
        }
        }

    for mensaje in mensajes:
        if mensaje != {'text': [u'']}:
            sendAPIfb(recipient_id, mensaje, url)
            time.sleep(1)
    elements = []
    """
    for sugerida in sugeridas:
        elements.append({
                "title":sugerida['pregunta'],
                "subtitle":sugerida['respuesta_plain_text'][:70]+'...',
                "buttons":[
                  {
                    "type":"postback",
                    "title":"View",
                    "payload":sugerida['pregunta'],
                  }
                ]
              })
    if sugeridas!=[]:
        sendAPIfb(recipient_id,
        {"attachment":{
                  "type":"template",
                  "payload":{
                    "template_type":"generic",
                    "elements":elements
                  }
                }},url)
    """
    marca_visita(owner, info, 'facebook_enviado', '', '', dialogo)


def envia_slack(mensaje, owner, info, dialogo, sugeridas=[]):
    # COMUN:

    fb_page = info.FBpage
    recipient_id = info.facebookid
    url = 'https://graph.facebook.com/v2.6/me/messages?access_token=' + fb_page.access_token
    mensaje = mensaje.replace('href="//', 'href="https://')
    mensaje = mensaje.replace('href="/', 'href="https://my.marketeer.co/')
    mensaje = mensaje.replace('src="//', 'src="https://')
    mensaje = mensaje.replace('src="/', 'src="https://my.marketeer.co/')

    # mensaje=mensaje.replace('&gt;','>')

    texto = html_to_text(mensaje).encode('utf-8').strip().replace('\n \n', '\n')

    soup = BS(mensaje, 'html.parser')

    txt2 = []
    t1 = ''
    for s in soup.find_all('a', href=True):
        if 'javascript:' in s['href'] or 'mailto:' in s['href']:
            s.extract()
        else:
            urlShorter = Urls(url=s['href'], info=info)
            urlShorter.save()
            s["href"] = urlShorter.shorten_url()

    longitud = len(texto)
    txt = []
    tope, ini = 0, 0
    n = 0
    while (ini < longitud) and n < 50:
        n = n + 1
        tope = ini + 300
        ini2 = texto.rfind("\n", ini, tope)
        if ini2 == -1 or ini2 == ini:
            ini2 = tope
        txt.append(texto[ini:ini2])
        ini = ini2

    mensajes = []
    buttons = []
    for imgtag in soup.find_all('img'):
        try:
            mensajes.append({
                "attachment":
                    {
                        "type": "image",
                        "payload": {"url": imgtag['src']}
                    }
            })
        except:
            pass

    for link in soup.findAll('a', href=True):
        buttons.append({
            "type": "web_url",
            "title": html_to_text(link.contents[0].encode('utf-8')).capitalize(),
            "url": link['href'],
        })

    for sugerida in sugeridas:
        buttons.append({
            "type": "postback",
            "title": sugerida['pregunta'],
            "payload": sugerida['pregunta'],
        })

    for trozo in txt:
        mensajes.append({'text': trozo})
    if len(buttons) > 0:
        mensajes[-1] = {"attachment": {
            "type": "template",
            "payload": {
                "template_type": 'button',
                "text": mensajes[-1]['text'],
                "buttons": buttons[:3]
            }
        }
        }

    for mensaje in mensajes:
        if mensaje != {'text': [u'']}:
            sendAPIfb(recipient_id, mensaje, url)
            time.sleep(1)
    elements = []

    marca_visita(owner, info, 'facebook_enviado', '', '', dialogo)


def sendAPIfb(recipient_id, dict_message, url):
    try:
        dict_message['text'].decode('utf-8')
    except:
        try:
            dict_message['text'] = dict_message['text'].decode('latin1').encode('utf-8')
        except:
            pass
    payload = {"recipient": {"id": recipient_id}, "message": dict_message}
    # with open('facebook_send.log', 'w+') as f:
    #    print((payload),file=f)
    r = requests.post(url, headers={"content-type": "application/json"}, data=json.dumps(payload))
    # with open('facebook_send1.log', 'w+') as f:
    #    print((r.text),file=f)


def sendAPIfbtext(recipient_id, text, url):
    payload = {"recipient": {"id": recipient_id}, "message": {"text": text}}
    r = requests.post(url, headers={"content-type": "application/json"}, data=json.dumps(payload))


def sendAPIfbimg(recipient_id, src, url):
    payload = {"recipient": {"id": recipient_id}, "message": {"attachment": {"type": "image", "payload": {"url": src}}}}
    r = requests.post(url, headers={"content-type": "application/json"}, data=json.dumps(payload))


def envia_whatsapp(owner, tipo, telefono, mensaje, id):
    '''
        type_msg : Text = Imagen / mensaje / estado / notificaciГіn. Identifica que tipo de envГ­o se harГЎ
        clientkey : Text = codigo cliente hash
        userkey : Text = SesiГіn hash a la que pertenece
        respuesta : Text UTF8
        typing : Boolean = Si el agente esta escribiendo
        respuesta_id : Number

    '''

    # URL del api del whatsapp para enviar los datos
    URL_API_WHATSAPP = 'http://integration.delio-lm.com:9080/whatsapp/'

    data = {}
    try:
        if tipo == 'mensaje':
            data = {
                'type_msg': tipo,
                'clientkey': encriptar_user(owner.phoneWhatsapp),
                'userkey': (encriptar_user(str(telefono[1:]))),
                'respuesta': html_to_text(mensaje),
                'typing': False,
                'respuesta_id': id
            }

        r = requests.post(URL_API_WHATSAPP, data=data)  # , headers=cabeceras)
        if r.status_code == 200:
            dialogo = Dialogos.objects.get(id=id)
            dialogo.device = 'WhatsApp'
            dialogo.save()
            return r.text

    except:
        pass


@csrf_exempt
def chat_whatsapp(request):
    '''
    type_msg : Text = Imagen / mensaje / estado / notificaciГіn. Identifica que tipo de envГ­o se harГЎ
    clientkey : Text = codigo cliente hash
    userkey : Text = Session hash / NГєmero de telГ©fono
    mensaje : Text UTF8 = Lo que escribe el usuario en UTF8
    typing : Boolean = Cuando estГЎ escribiendo
    mensaje_id : Number

    '''
    # JQUERY
    params = request.GET
    tipo = params['type_msg']
    mensaje = params['mensaje']
    typing = params['typing']
    mensaje_id = params['mensaje_id']
    # ------------------------------------
    # Desencripta el id de usuario:
    cambio_op = etiqueta(etiqueta_cambio, params['clientkey'])
    cambios(cambio_op, str(datetime.datetime.now()))
    try:
        idioma = params['idioma']  # :TODO
    except:
        idioma = 'en'
    phoneWhatsapp = desencriptar_user(params['clientkey'])
    owner = UsuarioVicloning.objects.filter(phoneWhatsapp=phoneWhatsapp)[0]
    marketeer = str(owner.user)
    mktrid = 'mktr' + str(owner.id)

    z = phonenumbers.parse(str(desencriptar_user(params['userkey'])), 'ES')
    phone = phonenumbers.format_number(z, phonenumbers.PhoneNumberFormat.E164)
    sesion = phone
    # email = whoIam(sesion,marketeer)

    email = whoIam(sesion, marketeer)
    try:
        info = Info.objects.filter(owner=owner, phone=phone)[0]
        email = info.email
    except:
        pass
    if not email:
        email = sesion + '@WhatsApp'
    # Obtiene la sala de chat

    defaults = {'idioma': idioma}

    try:
        sala, sala_created = Sala.objects.get_or_create(
                owner=owner, user_email=email, defaults=defaults)
    except:
        sala = Sala.objects.filter(owner=owner, user_email=email)
        for a in sala[1:]:
            a.delete()
        sala = sala[0]

    sala.idioma = idioma
    sala.save()

    ip = ''  # :TODO buscar la manera de aГ±adir pais ciudad etc

    try:
        city = ''
    except:
        city = ''
    try:
        country = geocoder.description_for_number(z, "en")
    except:
        country = ''
    try:
        device = carrier.name_for_number(z, "en")
    except:
        device = ''
    url = ''
    info = Info.objects.filter(owner=owner, email=email)
    if info:
        info = info[0]
        info.idioma = idioma
        info.save()
    else:
        defaults = {'nombre': 'Visitor', 'idioma': sala.idioma}
        info, info_created = Info.objects.get_or_create(
                owner=owner, email=email, phone=phone,
                defaults=defaults)

    defaults = {'ip': ip, 'pais': country}
    sesion_obj, created = Sesion.objects.get_or_create(
            codigo=sesion, usuario_vicloning=owner, defaults=defaults)
    chats = []
    if mensaje != '':
        dialogo = Dialogos(
                url=url, room=sala, author='CHAT_MSG_AUTHOR_USER', content=mensaje,
                ip=ip, country=country, city=city, browser='WhatsApp', sesion_user=sesion, device='WhatsApp')
        dialogo.save()
        # Marcamos Contacto            ------------------------------##
        try:
            referrer = request.META['HTTP_REFERER']
        except:
            referrer = ''
        marca_visita(owner, info, 'whatsapp_recibido', referrer, '', dialogo)
        # ----------------------------------------------------------##
        try:
            info.mensajes_sin_leer = info.mensajes_sin_leer + 1
            info.save()
        except:
            info.mensajes_sin_leer = 1
            info.save()

    return HttpResponse(json.dumps(({'recibido': chats})), content_type="application/json")


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    if not ip:
        ip = get_ip(request)
    return ip


def prueba(request):
    return render_to_response('prueba.html', RequestContext(request))


def pregunta_valida(pregunta, idioma):
    languages = {
        # "dutch",
        # "finnish",
        'de': "german",
        "es": "spanish",
        # "turkish",
        # "danish",
        "en": "english",
        'fr': "french",
        # "hungarian",
        # "norwegian",
        "ru": "russian",
        # "swedish",
        'it': "italian",
        "pt": "portuguese"}
    try:
        tokens = set(nltk.tokenize.word_tokenize(pregunta)) - set(nltk.corpus.stopwords.words(languages[idioma]))
    except:
        return len(pregunta) > 10

    return len(tokens) > 0


def create_faq_dialogo(dialogo_respuesta):
    """
    faqidioma:
    ambito_id: "5595"
    codigo_idioma: "es"
    faq_id: "34768"
    faqidioma_id: "138555"
    modified: null
    pregunta: "Contratar mГЎs idiomas"
    privada: false
    relacionada: []
    relacionada_delete: []
    respuesta: "<p>Para contratar idiomas adicionales, por favor, contacta&nbsp;con nosotros.</p><p><img data-filename="images.jpg" src="http://127.0.0.1:8000/media/client_uploads/79/img1462259838.8471229.jpg" style="width: 80px;"><br></p>"
    url: ""
    user: "marketeer"
    valorable: false

    relacionada:
    relacionada: [{id: null, texto: "asd"}]
    user: "marketeer"

    Folder:
    id_pather: 5595
    name: "fgfgdg"
    user: "marketeer"
    """
    # from vifaqs.api import FaqInsertarResource
    # from vifaqs.api import EquivalenteResource
    # 2016-08-01 guarda una FAQ
    sala = dialogo_respuesta.room
    owner = sala.owner
    idioma = Idioma.objects.get(codigo=sala.idioma)
    anterior_a = dialogo_respuesta.created

    mensajes_operador_anteriores = Dialogos.objects.filter(room=sala, created__lte=anterior_a,
                                                           author='CHAT_MSG_AUTHOR_OPERATOR').order_by('-created')
    if len(mensajes_operador_anteriores) > 1:
        mensaje_operador_anterior = mensajes_operador_anteriores[1]
        preguntas = Dialogos.objects.filter(room=sala, created__lte=anterior_a,
                                            created__gt=mensaje_operador_anterior.created,
                                            author='CHAT_MSG_AUTHOR_USER').order_by('created')
    else:
        preguntas = Dialogos.objects.filter(room=sala, created__lte=anterior_a, author='CHAT_MSG_AUTHOR_USER').order_by(
            'created')

    pregunta = ''
    for trozo_pregunta in preguntas:
        pregunta = pregunta + trozo_pregunta.content + ' '
    # if pregunta_valida(pregunta,idioma.codigo) and len(dialogo_respuesta.content)>20:
    ambito, q = Ambito.objects.get_or_create(usuario_vicloning=owner, chat=True,
                                             padre=Ambito.objects.get(usuario_vicloning=owner, general=True))
    for idioma_extra in Idioma.objects.all():
        ambitoidioma, q = AmbitoIdioma.objects.get_or_create(ambito=ambito, nombre='chat', idioma=idioma_extra)
    pregunta.replace('/n', ' ')
    new_faq = FaqIdioma.objects.filter(faq__usuario_vicloning=owner, respuesta=dialogo_respuesta.content)
    if len(new_faq) > 0:
        # print '>>>> entro'
        if not Equivalente.objects.filter(faqidioma=new_faq[0], pregunta=pregunta, idioma=new_faq[0].idioma):
            # bundle={
            #    "relacionada":[{"id":new_faq[0].id,"texto":pregunta}],
            #    "user":owner.user.username
            # }
            equivalente = Equivalente(faqidioma=new_faq[0], pregunta=pregunta, idioma=new_faq[0].idioma)
            equivalente.save()
            # add_equivalente(equivalente)
    else:
        # bundle=''
        # bundle.data={
        #    "user":owner.user.username,
        #    "ambito_id":ambito.id,
        #    "codigo_idioma":idioma.codigo,
        #    "pregunta":pregunta,
        #    "privada":True,
        #    "relacionada":[],
        #    "respuesta":dialogo_respuesta.content,
        #    "url":"",
        #    "valorable":False,
        # }
        # FaqInsertarResource().obj_create(bundle)
        faq = Faq(usuario_vicloning=owner, ambito=ambito)
        faq.save()
        new_faq = FaqIdioma(faq=faq, pregunta=pregunta, idioma=idioma, privada=True,
                            respuesta=dialogo_respuesta.content)
        add_faqidioma(new_faq)
        new_faq.save()


def whatisthis(s):
    if isinstance(s, str):
        a = "ordinary string"
    elif isinstance(s, unicode):
        a = "unicode string"
    else:
        a = "not a string"
    return false
